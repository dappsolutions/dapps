var Home = {
 sendContact: function (elm, e) {
  e.preventDefault();
  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: {
     nama: $('#nama').val(),
     subjek: $('#subject').val(),
     email: $('#email').val(),
     pesan: $('#message').val(),
    },
    dataType: 'json',
    async: false,
    url: url.base_url("template_home") + "sendContact",
    error: function () {
     bootbox.dialog({
      message: "Gagal"
     });
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     message.closeLoading();
     if (resp.is_valid) {
//     bootbox.dialog({
//      message: "Berhasil Dikirim"
//     });
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      bootbox.dialog({
       message: "Gagal Dikirim"
      });
     }
    }
   });
  }
 }
};