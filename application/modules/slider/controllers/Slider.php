<?php

class Slider extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'slider';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/slider.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'slider';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Slider";
  $data['title_content'] = 'Data Slider';
  $content = $this->getDataSlider();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataSlider($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('s.foto', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' s',
              'field' => array('s.*'),
              'like' => $like,
              'is_or_like' => true,
              'where' => "s.deleted = 0"
  ));

  return $total;
 }

 public function getDataSlider($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('s.foto', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' s',
              'field' => array('s.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataSlider($keyword)
  );
 }

 public function getDetailDataSlider($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'where' => "kr.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListOrganisasi() {
  $data = Modules::run('database/get', array(
              'table' => 'organisasi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Slider";
  $data['title_content'] = 'Tambah Slider';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataSlider($id);
  $data['foto'] = str_replace(' ', '_', $data['foto']);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Slider";
  $data['title_content'] = 'Ubah Slider';
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusSlider($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataSlider($id);

  $data['foto'] = str_replace(' ', '_', $data['foto']);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Slider";
  $data['title_content'] = "Detail Slider";
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function getPostDetail($masjid, $value) {
  $data['masjid'] = $masjid;
  $data['organisasi'] = $value->organisasi;
  return $data;
 }

 public function getDataSliderOrganisasi($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi',
              'where' => array('masjid' => $masjid)
  ));

  $is_exist = false;
  if (!empty($data)) {
   $is_exist = true;
  }

  return $is_exist;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post_slider = $this->getPostDataHeader($data);
   $is_uploaded = true;
   if ($id == '') {
    if (!empty($_FILES)) {
     $data_file = $this->uploadData("file");
     $is_uploaded = $data_file['is_valid'];
     if ($is_uploaded) {
      $post_slider['foto'] = $_FILES['file']['name'];
     } else {
      $is_valid = false;
      $message = $data_file['response'];
     }
    }
    if ($is_uploaded) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post_slider);
    }
   } else {
    //update
    if (!empty($_FILES)) {
     $data_file = $this->uploadData("file");
     $is_uploaded = $data_file['is_valid'];
     if ($is_uploaded) {
      $post_slider['foto'] = $_FILES['file']['name'];
     } else {
      $is_valid = false;
      $message = $data_file['response'];
     }
    }
    if ($is_uploaded) {
     Modules::run('database/_update', $this->getTableName(), $post_slider, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message'=> $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Slider";
  $data['title_content'] = 'Data Slider';
  $content = $this->getDataSlider($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/slider/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function getKoordinatSekarang($id = '') {
  if ($id == '') {
   $id = $this->session->userdata('masjid_id');
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName(),
              'where' => array('id' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

}
