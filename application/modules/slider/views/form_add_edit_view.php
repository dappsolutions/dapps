<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content-wrapper"> 
 <div class="row">
  <div class='col-12'>
   <div class="card">       
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>

     <div class='row manual_add <?php echo!isset($foto) ? '' : 'display-none' ?>'>            
      <div class='col-md-3'>
       File Slider
      </div>
      <div class='col-md-4'>
       <div class="fileinput fileinput-new input-group manual_upload" data-provides="fileinput">
        <div class="form-control" data-trigger="fileinput"> 
         <i class="glyphicon glyphicon-file fileinput-exists"></i> 
         <span class="fileinput-filename"></span>
        </div> 
        <span class="input-group-addon btn btn-default btn-file bg-primary text-white"> 
         <span class="fileinput-new" onclick="Slider.upload(this)">Select file</span> 
         <input type="file" style="display: none;" id="file" onchange="Slider.getFilename(this)"/>
        </span> 
       </div>
      </div>
     </div>
     <div class='row manual_detail <?php echo!isset($foto) ? 'display-none' : '' ?>'>
      <div message="Ganti Foto" class='col-md-3 '>
       <?php
       $img = base_url() . 'assets/images/no_available.png';
       if (isset($foto)) {
        if ($foto == '') {
         $img = base_url() . 'assets/images/no_available.png';
        } else {
         $img = base_url() . 'files/berkas/slider/' . $foto;
        }
       }
       ?>
       <img data-toggle="tooltip" title="Klik untuk Ganti File" data-placement="bottom" src="<?php echo $img ?>" height="180" width="180" class="hover" onclick="Slider.changeManual(this)"/>       
      </div>
      <div class='col-md-4'>
       <a href="#" onclick="Slider.showLogo(this, event)" class="badge badge-danger"><?php echo $foto ?></a>        
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='keterangan' class='form-control required' 
              value='<?php echo isset($keterangan) ? $keterangan : '' ?>' error="Keterangan"/>
      </div>     
     </div>
     <br/>

     <!--<hr/>-->
     <div class='row'>
      <div class='col-md-7 text-right'>
       <span id="" class="btn btn-success hover" onclick="Slider.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</span>
       &nbsp;
       <span id="" class="btn btn-warning hover" onclick="Slider.back()">Kembali</span>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
