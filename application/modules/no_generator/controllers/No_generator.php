<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class No_generator extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateInvoice($frontID,$table) {
  $no_invoice = $frontID . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => $table,
  'like' => array(
  array('no_invoice', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_invoice']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  echo $no_invoice;
 }

}
