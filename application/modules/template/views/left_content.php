<!-- partial:partials/_sidebar.html -->
<nav class="sidebar" id="sidebar">
 <ul class="nav">
  <li class="nav-item nav-profile">
   <div class="nav-link">
    <div class="user-wrapper">
     <hr/>
     <div class="profile-image">
      <img src="<?php echo base_url() ?>assets/images/faces-clipart/pic-4.png" alt="profile image">
     </div>
     <div class="text-wrapper">
      <p class="profile-name"><?php echo strtoupper($this->session->userdata('username')); ?></p>
      <div>
       <small class="designation text-muted"><?php echo $this->session->userdata('hak_akses'); ?></small>
       <span class="status-indicator online"></span>
      </div>
     </div>
    </div>
    <button class="btn btn-success btn-block" onclick="Template.homeApps(this)">Mitra Kubah
     <!--<i class="mdi mdi-plus"></i>-->
    </button>
   </div>
  </li>

  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'dashboard' ?>">
    <i class="menu-icon mdi mdi-television"></i>
    <span class="menu-title">Dashboard</span>
   </a>
  </li>
  
  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'slider' ?>">
    <i class="menu-icon mdi mdi-file-image"></i>
    <span class="menu-title">Slider</span>
   </a>
  </li>
  
  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'gallery' ?>">
    <i class="menu-icon mdi mdi-image"></i>
    <span class="menu-title">Gallery</span>
   </a>
  </li>
  
  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'blog' ?>">
    <i class="menu-icon mdi mdi-blogger"></i>
    <span class="menu-title">Blog</span>
   </a>
  </li>
  
  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'produk' ?>">
    <i class="menu-icon mdi mdi-note"></i>
    <span class="menu-title">Produk</span>
   </a>
  </li>
  
  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'pelayanan' ?>">
    <i class="menu-icon mdi mdi-rename-box"></i>
    <span class="menu-title">Pelayanan</span>
   </a>
  </li>
  
  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'kontak' ?>">
    <i class="menu-icon mdi mdi-message"></i>
    <span class="menu-title">Kotak Masuk</span>
   </a>
  </li>
  
  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'whatsapp' ?>">
    <i class="menu-icon mdi mdi-whatsapp"></i>
    <span class="menu-title">Wa Pesan</span>
   </a>
  </li>
  
  <li class="nav-item">
   <a class="nav-link" href="<?php echo base_url() . 'tentang' ?>">
    <i class="menu-icon mdi mdi-equal-box"></i>
    <span class="menu-title">Tentang</span>
   </a>
  </li>
  
 </ul>
</nav>
<!-- partial -->