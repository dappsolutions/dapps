<html>
 <?php echo Modules::run('header'); ?>
 <?php
 if (isset($header_data)) {
  foreach ($header_data as $v_head) {
   echo $v_head;
  }
 }
 ?>
 <body>
<!--  <div class="preloader">
   <svg class="circular" viewBox="25 25 50 50">
   <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
  </div>-->

  <div id="container-scroller">
   <div class='loader'>

   </div>
   <?php echo $this->load->view('header_content'); ?>
   <div class="container-fluid page-body-wrapper">
    <div class="loader"></div>
    <?php echo $this->load->view('left_content'); ?>
    <div class='main-panel'>
     <?php echo $this->load->view($module . '/' . $view_file); ?>
     <?php echo $this->load->view('footer_content'); ?>
    </div>     
   </div>
  </div>
 </body>
</html>
