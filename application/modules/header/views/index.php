<head>
 <!-- Required meta tags -->
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <title><?php echo isset($title) ? $title : '' ?></title>
 <!-- plugins:css -->
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/jquery-ui.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/vendor.bundle.base.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/vendor.bundle.addons.css">
 
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/classic.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/classic.date.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/classic.time.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/mdi/css/materialdesignicons.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/css-loader.css">
 <!-- endinject -->
 <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png" />
</head>

<script src="<?php echo base_url() ?>assets/js/vendor.bundle.base.js"></script>
<script src="<?php echo base_url() ?>assets/js/vendor.bundle.addons.js"></script>

<!--<script src="<?php echo base_url() ?>assets/js/jquery_min_latest.js"></script>
  <script src="<?php echo base_url() ?>assets/js/moment_min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/daterangepicker.js"></script>
  <script src="<?php echo base_url() ?>assets/css/daterangepicker.css"></script>-->
<script src="<?php echo base_url() ?>assets/js/picker.js"></script>
<script src="<?php echo base_url() ?>assets/js/picker.date.js"></script>
<script src="<?php echo base_url() ?>assets/js/picker.time.js"></script>

<script src="<?php echo base_url() ?>assets/js/off-canvas.js"></script>
<script src="<?php echo base_url() ?>assets/js/misc.js"></script>
<script src="<?php echo base_url() ?>assets/js/Chart.bundle.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/utils.js"></script>
<!-- endinject -->

<script src="<?php echo base_url() ?>assets/js/dashboard.js"></script>
<script src="<?php echo base_url() ?>assets/js/url.js"></script>
<script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootbox.js"></script>
<script src="<?php echo base_url() ?>assets/js/message.js"></script>
<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
<script src="<?php echo base_url() ?>assets/js/controllers/template.js"></script>