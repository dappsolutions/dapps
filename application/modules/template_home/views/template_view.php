<!doctype html>
<html lang="en">

 <?php echo Modules::run('header_home') ?>
 <body>

  <?php echo $this->load->view('header_view') ?>

  <?php echo $this->load->view($module . '/' . $view_file) ?>

  <?php echo $this->load->view('footer_view'); ?>
  <?php echo Modules::run('js_home') ?>
 </body>

</html>