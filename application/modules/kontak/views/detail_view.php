<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content-wrapper"> 
 <div class="row">
  <div class='col-12'>
   <div class="card">       
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>

     <div class="row">
      <div class='col-md-3'>
       Nama
      </div>
      <div class='col-md-4 text-primary'>
       <?php echo $nama  ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Email
      </div>
      <div class='col-md-4 text-primary'>
       <?php echo $email  ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Subject
      </div>
      <div class='col-md-4 text-primary'>
       <?php echo $subject  ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Pesan
      </div>
      <div class='col-md-4 text-primary'>
       <?php echo $message  ?>
      </div>     
     </div>
     <br/>

     <!--<hr/>-->
     <div class='row'>
      <div class='col-md-7 text-right'>
       <span id="" class="btn btn-warning hover" onclick="Kontak.back()">Kembali</span>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
