
<div class="content-wrapper">
 <div class="row">
  <div class="col-md-12">
   <h4>Kehadiran Jamaah Sholat Wajib (<?php echo date('Y-m-d') ?>)</h4>
   <br/>
   <input type='hidden' name='' id='data_kehadiran' class='form-control' value='<?php echo $data_jamaah['data'] ?>'/>
   <input type='hidden' name='' id='total_data_kehadiran' class='form-control' value='<?php echo $data_jamaah['total'] ?>'/>
   <canvas id="canvas_jamaah"></canvas>
  </div>   
 </div>  
</div>