
<div class="content-wrapper">
 <div class="row"> 
  <div class="col-md-12">   
   <br/>
   <div class='card'>
    <div class='card-body'>
     <div id='map' style="height: 300px;">

     </div>
     <br/>

     <div class=''>
      <div>
       <strong>Start: </strong>
       <select id="start" onchange="calcRoute();">
        <option value="chicago, il">Chicago</option>
        <option value="st louis, mo">St Louis</option>
        <option value="joplin, mo">Joplin, MO</option>
        <option value="oklahoma city, ok">Oklahoma City</option>
        <option value="amarillo, tx">Amarillo</option>
        <option value="gallup, nm">Gallup, NM</option>
        <option value="flagstaff, az">Flagstaff, AZ</option>
        <option value="winona, az">Winona</option>
        <option value="kingman, az">Kingman</option>
        <option value="barstow, ca">Barstow</option>
        <option value="san bernardino, ca">San Bernardino</option>
        <option value="los angeles, ca">Los Angeles</option>
       </select>
       <strong>End: </strong>
       <select id="end" onchange="calcRoute();">
        <option value="chicago, il">Chicago</option>
        <option value="st louis, mo">St Louis</option>
        <option value="joplin, mo">Joplin, MO</option>
        <option value="oklahoma city, ok">Oklahoma City</option>
        <option value="amarillo, tx">Amarillo</option>
        <option value="gallup, nm">Gallup, NM</option>
        <option value="flagstaff, az">Flagstaff, AZ</option>
        <option value="winona, az">Winona</option>
        <option value="kingman, az">Kingman</option>
        <option value="barstow, ca">Barstow</option>
        <option value="san bernardino, ca">San Bernardino</option>
        <option value="los angeles, ca">Los Angeles</option>
       </select>
      </div>
     </div>
    </div>
   </div>

   <script>
    var map;
//    function initMap() {
//     // The location of Uluru
//     var uluru = {lat: -25.344, lng: 131.036};
//     // The map, centered at Uluru
//     var map = new google.maps.Map(
//             document.getElementById('map'), {zoom: 4, center: uluru});
//     // The marker, positioned at Uluru
//     var marker = new google.maps.Marker({position: uluru, map: map});
//    }

    var directionsDisplay;
    var directionsService;
    function initMap() {
     directionsService = new google.maps.DirectionsService();
     directionsDisplay = new google.maps.DirectionsRenderer();
     var chicago = new google.maps.LatLng(41.850033, -87.6500523);
     var mapOptions = {
      zoom: 7,
      center: chicago
     }
     var map = new google.maps.Map(document.getElementById('map'), mapOptions);
     directionsDisplay.setMap(map);
    }

    function calcRoute() {
     var start = document.getElementById('start').value;
     var end = document.getElementById('end').value;
     var request = {
      origin: start,
      destination: end,
      travelMode: 'DRIVING'
     };
     directionsService.route(request, function (result, status) {
      console.log("data direction",result);
      if (status == 'OK') {
       directionsDisplay.setDirections(result);
      }
     });
    }
   </script>

   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfYn4-6FITJt3IS5qAnnY09I3mOGJSX18&callback=initMap"
   async defer></script>
  </div>     
 </div>  
</div>