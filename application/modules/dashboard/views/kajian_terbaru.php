<div class='content-wrapper'>
 <div class="row">
  <!-- column -->
  <div class="col-12">
   <div class="card">
    <div class="card-body">
     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table table-bordered table-striped">
         <thead>
          <tr>
           <th>No</th>
           <th>Nama Masjid</th>
           <th>Pengisi Kajian</th>
           <th>Nama Kajian</th>
           <th>Jadwal Kajian</th>           
           <th>Status</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr>
             <td><?php echo $no++ ?></td>
             <td><?php echo $value['nama_masjid'] ?></td>
             <td><?php echo $value['pengisi_kajian'] ?></td>
             <td><?php echo $value['nama_kajian'] ?></td>
             <td><?php echo $value['jadwal'] . ' ' . $value['jam'] ?></td>
             <td><?php echo $value['status'] ? 'Aktif' : 'Non Aktif' ?></td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td class="text-center" colspan="7">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>
     </div>    
    </div>
   </div>
  </div>
 </div> 
</div>