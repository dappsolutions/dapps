
<div class="content-wrapper">
 <div class="row"> 
  <div class="col-md-12">
   <h4>Tampilkan kehadiran jamaah kajian per masjid (<?php echo date('Y-m-d') ?>)</h4>
   <br/>
   <input type='hidden' name='' id='data_kajian' class='form-control' value='<?php echo $data_kajian['data'] ?>'/>
   <input type='hidden' name='' id='total_data_kajian' class='form-control' value='<?php echo $data_kajian['total'] ?>'/>
   <canvas id="canvas_kajian"></canvas>
  </div>     
 </div>  
</div>