<input type='hidden' name='' id='hak_akses' class='form-control' value='<?php echo $this->session->userdata('hak_akses') ?>'/>
<div class="content-wrapper">
 <div class="row">
  <div class="col-md-12">
   <div class="card">
    <div class="card-body">
     <h1 class="text-warning">Manajemen Admin Web Mitra Quubah</h1>
    </div>
   </div>
  </div>
 </div>
 <br/>

 <div class="row">
  <div class="col-md-12">
   <div class="card">
    <div class="card-body">
     <h4 class="">Top <label class="badge badge-success">3</label> Data Slider</h4>

     <div class="table-responsive">
      <table class="table table-bordered">
       <thead class="bg-success text-white">
        <tr class="font-12">
         <th class="font-12">No</th>
         <th class="font-12">Nama Slider</th>
         <th class="font-12">Keterangan</th>
         <th class="font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($slider)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($slider as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['foto'] ?></td>
           <td class='font-12'><?php echo $value['keterangan'] ?></td>
           <td class="text-center font-12">
            <label id="" class="btn btn-success font-12 hover" 
                   onclick="Slider.detail('<?php echo $value['id'] ?>')">Detail</label>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
 <br/>

 <div class="row">
  <div class="col-md-12">
   <div class="card">
    <div class="card-body">
     <h4 class="">Top <label class="badge badge-success">3</label> Data Produk</h4>
     <div class="table-responsive">
      <table class="table table-bordered">
       <thead class="bg-success text-white">
        <tr class="font-12">
         <th class="font-12">No</th>
         <th class="font-12">Nama Produk</th>
         <th class="font-12">Foto</th>
         <th class="font-12">Deskripsi</th>
         <th class="font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($produk)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($produk as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['nama'] ?></td>
           <td class='font-12'><?php echo $value['foto'] ?></td>
           <td class='font-12'><?php echo $value['deskripsi'] ?></td>
           <td class="text-center font-12">
            <label id="" class="btn btn-success font-12 hover" 
                   onclick="Slider.detail('<?php echo $value['id'] ?>')">Detail</label>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
 <br/>

 <div class="row">
  <div class="col-md-12">
   <div class="card">
    <div class="card-body">
     <h4 class="">Top <label class="badge badge-success">3</label> Data Pelanggan Kontak</h4>
     <div class="table-responsive">
      <table class="table table-bordered">
       <thead class="bg-success text-white">
        <tr class="font-12">
         <th class="font-12">No</th>
         <th class="font-12">Nama</th>
         <th class="font-12">Email</th>
         <th class="font-12">Subject</th>
         <th class="font-12">Pesan</th>
         <th class="font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($kontak)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($kontak as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['nama'] ?></td>
           <td class='font-12'><?php echo $value['email'] ?></td>
           <td class='font-12'><?php echo $value['subject'] ?></td>
           <td class='font-12'><?php echo $value['message'] ?></td>
           <td class="text-center font-12">
            <label id="" class="btn btn-success font-12 hover" 
                   onclick="Slider.detail('<?php echo $value['id'] ?>')">Detail</label>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
 <br/>
</div>