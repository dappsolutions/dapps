<?php

class Dashboard extends MX_Controller {

 public $masjid;
 public $organisasi;
 public $hak_akses;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
  $this->masjid = $this->session->userdata('masjid_id');
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->organisasi = $this->session->userdata('org_id');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['slider'] = $this->getDataSlider();
  $data['produk'] = $this->getDataProduk();
  $data['kontak'] = $this->getDataKontak();
  echo Modules::run('template', $data);
 }

 public function getDataSlider() {
  $data = Modules::run('database/get', array(
              'table' => 'slider s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0",
              'limit' => 3,
              'orderby'=> 's.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }
 
 public function getDataProduk() {
  $data = Modules::run('database/get', array(
              'table' => 'produk s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0",
              'limit' => 3,
              'orderby'=> 's.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }
 
 public function getDataKontak() {
  $data = Modules::run('database/get', array(
              'table' => 'kontak s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0",
              'limit' => 3,
              'orderby'=> 's.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

}
