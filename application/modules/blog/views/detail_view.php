<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content-wrapper"> 
 <div class="row">
  <div class='col-12'>
   <div class="card">       
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>

     <div class='row manual_detail <?php echo!isset($foto) ? 'display-none' : '' ?>'>
      <div class='col-md-3 '>
       <?php
       $img = base_url() . 'assets/images/no_available.png';
       if (isset($foto)) {
        if ($foto == '') {
         $img = base_url() . 'assets/images/no_available.png';
        } else {
         $img = base_url() . 'files/berkas/blog/' . urldecode($foto);
        }
       }
       ?>
       <img src="<?php echo $img ?>" height="180" width="180"/>       
      </div>
      <div class='col-md-4'>
       <a href="#" onclick="Blog.showLogo(this, event)" class="badge badge-danger"><?php echo $foto ?></a>        
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Judul
      </div>
      <div class='col-md-4 text-primary'>
       <?php echo $judul  ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Deskripsi
      </div>
      <div class='col-md-4 text-primary'>
       <?php echo $keterangan  ?>
      </div>     
     </div>
     <br/>

     <!--<hr/>-->
     <div class='row'>
      <div class='col-md-7 text-right'>
       <span id="" class="btn btn-warning hover" onclick="Blog.back()">Kembali</span>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
