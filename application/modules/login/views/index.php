<!DOCTYPE html>
<html lang="en">

 <head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Login Admin Quubah</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png" />
 </head>

 <body>
  <div class="container-scroller">
   <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
    <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
     <div class="row w-100">
      <div class="col-lg-4 mx-auto">
       <div class="auto-form-wrapper">
        <form action="#">
         <div class="form-group">
          <label class="label">Username</label>
          <div class="input-group">
           <input type="text" error='Username' class="form-control required" placeholder="Username" id='username'>
           <div class="input-group-append">
            <span class="input-group-text">
             <i class="mdi mdi-check-circle-outline"></i>
            </span>
           </div>
          </div>
         </div>
         <div class="form-group">
          <label class="label">Password</label>
          <div class="input-group">
           <input type="password" error='Password' class="form-control required" placeholder="*********" id='password'>
           <div class="input-group-append">
            <span class="input-group-text">
             <i class="mdi mdi-check-circle-outline"></i>
            </span>
           </div>
          </div>
         </div>
         <div class="form-group">
          <button onclick="Login.sign_in(this, event)" class="btn btn-primary submit-btn btn-block">Login</button>
         </div>
         <div class="form-group d-flex justify-content-between">
<!--          <div class="form-check form-check-flat mt-0">
           <label class="form-check-label">
            <input type="checkbox" class="form-check-input" checked> Keep me signed in
           </label>
          </div>-->
          <!--<a href="#" class="text-small forgot-password text-black">Forgot Password</a>-->
         </div>
        </form>
       </div>
       <p class="footer-text text-center">Sahabat Masjid Indonesia</p>
      </div>
     </div>
    </div>
    <!-- content-wrapper ends -->
   </div>
   <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?php echo base_url() ?>assets/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url() ?>assets/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?php echo base_url() ?>assets/js/off-canvas.js"></script>
  <script src="<?php echo base_url() ?>assets/js/misc.js"></script>
  <script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/url.js"></script>
  <script src="<?php echo base_url() ?>assets/js/message.js"></script>
  <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
  <script src="<?php echo base_url() ?>assets/js/controllers/login.js"></script>
  <!-- endinject -->
 </body>

</html>